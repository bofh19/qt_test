#include "songitemmodel.h"
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonDocument>
SongItemModel::~SongItemModel()
{
    qDebug() << "SongItemModel destructor called ";
}

SongItemModel::SongItemModel(QObject* parent) : QAbstractListModel(parent)
{
    manager = new QNetworkAccessManager(this);
    SongItemModel::connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(networkReplyFetched(QNetworkReply*)));
    QUrl url("http://backpapermusic.w4rlock.in/api/music_list/");
    getNetworkData(url);
}

int SongItemModel::rowCount(const QModelIndex &parent) const
{
    return song_items_.size();
}

QVariant SongItemModel::data(const QModelIndex &index, int role) const
{
    // Check that the index is valid and within the correct range first:
    if (!index.isValid()) return QVariant();
    if (index.row() >= song_items_.size()) return QVariant();

    if (role == Qt::DisplayRole) {
        QString qstr = (song_items_.at(index.row()))->getSongDisplayName();
        return QVariant(qstr);
    }else if(role == Qt::UserRole)
    {
        SongItem i( song_items_.at(index.row()));
        QVariant val = QVariant::fromValue(i);
        return val;
    }
    else{
        return QVariant();
    }
}


SongItem* SongItemModel::getSongItem(int index)
{
    return song_items_.at(index);
}

void SongItemModel::getNetworkData(QUrl url)
{
    QNetworkReply* reply = manager->get(QNetworkRequest(url));
    reply->setParent(this);
    SongItemModel::connect(reply,SIGNAL(downloadProgress(qint64,qint64)),this,SIGNAL(dataProgress(qint64,qint64)));
}

void SongItemModel::networkReplyFetched(QNetworkReply* reply)
{
    qDebug() << "in reply from populate network data class";

    if (reply->error() == QNetworkReply::NoError)
    {

        qDebug() << "no errors recieved";
        QString data = (QString)reply->readAll();
        QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8());
        QJsonArray mainArray = doc.array();
        qDebug() << "mainArray.size() "<<mainArray.size();
        beginResetModel();
        for(int i=0;i<mainArray.size();i++){
            QJsonArray arraytwo(mainArray.at(i).toArray());
            for(int j=0;j<arraytwo.size();j++){
                QJsonObject jsonObj(arraytwo.at(j).toObject());
                QJsonValue value1(jsonObj.value("name"));
                QJsonValue value2(jsonObj.value("track_url"));
                QJsonValue value3(jsonObj.value("album"));
                QJsonValue value4(jsonObj.value("thumb_url"));

                QJsonValue value5(jsonObj.value("album"));
                QJsonValue value6(jsonObj.value("name"));
                QJsonValue value7(jsonObj.value("artist"));
                SongItem *song_item = new SongItem();
                song_item->setSongDisplayName(value3.toString()+" / "+value1.toString());
                song_item->setSongUrl(value2.toString());
                song_item->setongThumbUrl(value4.toString());
                song_item->setAlbumName(value5.toString());
                song_item->setName(value6.toString());
                song_item->setArtist(value7.toString());
                song_items_.append(song_item);
            }
        }
        endResetModel();
        qDebug() << "model set";
    }
    else
    {
        qDebug() << "Errors recieved";
        qDebug() << reply->errorString();
        // handle errors here
    }
    delete reply;

}
