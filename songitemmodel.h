#ifndef SONGITEMMODEL_H
#define SONGITEMMODEL_H
#include <QAbstractListModel>
#include "songitem.h"
#include <QList>
#include <QNetworkReply>
#include <QNetworkAccessManager>
class SongItemModel : public QAbstractListModel
{
Q_OBJECT
public:
    explicit SongItemModel(QObject* parent);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    SongItem* getSongItem(int index);
    void serachItem(QString arg);
    void getNetworkData(QUrl url);

    ~SongItemModel();

private:
    QList<SongItem*> song_items_;
    QNetworkAccessManager *manager;

signals:
    void dataProgress(qint64 bytesReceived,qint64 bytesTotal);

public slots:
    void networkReplyFetched(QNetworkReply* reply);
};

#endif // SONGITEMMODEL_H
