#ifndef SONGITEM_H
#define SONGITEM_H
#include <QString>
#include <QMetaType>
class SongItem
{
public:
    SongItem();
    SongItem(const SongItem &other);
    SongItem(SongItem* other);
    ~SongItem();
    QString getSongDisplayName() const;
    QString getSongDisplayName2();
    QString getSongUrl() const;
    void setSongDisplayName(QString str);
    void setSongUrl(QString url);
    QString getServerSongUrl() const;
    QString getSongThumbUrl() const;
    void setongThumbUrl(QString url);
    QString getServerSongThumbUrl();

    QString getAlbumName() const;
    void setAlbumName(QString album_name);

    QString getName() const;
    void setName(QString name);

    QString getArtist() const;
    void setArtist(QString artist);

private:
    QString m_songUrl,m_songDisplayName,m_song_thumb_url,m_album,m_name,m_artist;
};

Q_DECLARE_METATYPE(SongItem)
#endif // SONGITEM_H
