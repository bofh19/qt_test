#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "populatenetworkdata.h"
#include "songitemmodel.h"
#include <QUrl>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    player = new QMediaPlayer;

    connect(player,SIGNAL(durationChanged(qint64)),this,SLOT(slot_duration_changed(qint64)));
    connect(player,SIGNAL(volumeChanged(int)),this,SLOT(slot_volume_changed(qint64)));
    connect(player,SIGNAL(positionChanged(qint64)),this,SLOT(slot_position_changed(qint64)));

    SongItemModel *songItemModel = new SongItemModel(this);
    connect(songItemModel,SIGNAL(dataProgress(qint64,qint64)),this,SLOT(dataProgress(qint64,qint64)));
    songItemModelProxy = new QSortFilterProxyModel(this);
    songItemModelProxy->setSourceModel(songItemModel);
    connect(player,SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),this,SLOT(slot_media_status_changed(QMediaPlayer::MediaStatus)));
    ui->listView->setModel(songItemModelProxy);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::dataProgress(qint64 bytesReceived,qint64 bytesTotal)
{
    qDebug() << "bytesReceived " << bytesReceived << " bytesTotal "<<bytesTotal;
    QString statusMessage = QString("Loaded Data ... %1KB").arg(bytesReceived/1024);
    ui->statusBar->showMessage(statusMessage);
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
    qDebug() << "Text Changed "<<arg1;
    QRegExp regExp(QString("*%1*").arg(arg1), Qt::CaseInsensitive,QRegExp::Wildcard);
    songItemModelProxy->setFilterRegExp(regExp);
}


void MainWindow::playMusic(SongItem songItem)
{
    qDebug() << "trying to play from "<<songItem.getServerSongUrl();
    player->setMedia(QUrl(songItem.getServerSongUrl()));
    player->setVolume(100);
    player->play();

    QNetworkAccessManager *m_netwManager = new QNetworkAccessManager(this);
    connect(m_netwManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(slot_thumbFetched(QNetworkReply*)));
    qDebug() << "Song thumb url: "<<songItem.getServerSongThumbUrl();

    QUrl url(songItem.getServerSongThumbUrl());
    QNetworkRequest request(url);
    m_netwManager->get(request);

    ui->song_label->setText(songItem.getSongDisplayName2());
}

void MainWindow::slot_thumbFetched(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError) {
            qDebug() << "Error in" << reply->url() << ":" << reply->errorString();
            return;
        }

        QByteArray jpegData = reply->readAll();
        QPixmap pixmap;
        pixmap.loadFromData(jpegData);
        pixmap = pixmap.scaled(ui->imageView->width(),ui->imageView->height(),Qt::KeepAspectRatio,Qt::FastTransformation);
        ui->imageView->setPixmap(pixmap);

}

void MainWindow::on_listView_clicked(const QModelIndex &index)
{
    qDebug() << "clicked on "<<index.row();
    QVariant val = songItemModelProxy->data(index,Qt::UserRole);
    SongItem retrieved = val.value<SongItem>();
    playMusic(retrieved);
}

void MainWindow::on_playButton_clicked()
{
    player->play();
}

void MainWindow::on_pauseButton_clicked()
{
    player->pause();
}

void MainWindow::slot_duration_changed(qint64 duration)
{
    qDebug() << player->duration() << " "<<duration;
    ui->seek_bar->setMaximum(duration);
}

void MainWindow::slot_volume_changed(qint64 position)
{
    qDebug()  << " "<<position;
}

void MainWindow::slot_position_changed(qint64 duration)
{
    ui->seek_bar->setValue(duration);
    qint64 currentInfo = duration/1000;
//    qDebug() << player->duration() << " "<<duration;
    QString tStr;
    if (duration)
    {
        QTime currentTime((currentInfo/3600)%60, (currentInfo/60)%60, currentInfo%60, (currentInfo*1000)%1000);
        QTime totalTime((duration/3600)%60, (duration/60)%60, duration%60, (duration*1000)%1000);
        QString format = "mm:ss";
        if (duration > 3600)
            format = "hh:mm:ss";
        tStr = currentTime.toString(format) ;//+ " / " + totalTime.toString(format);
    }
    ui->song_time->setText(tStr);
}

void MainWindow::slot_media_status_changed(QMediaPlayer::MediaStatus status)
{
    qDebug() << "Status: "<<status;
}

void MainWindow::on_seek_bar_sliderMoved(int position)
{
    qDebug() << "seekbar slider moved: "<<position;
    player->setPosition(position);
}
