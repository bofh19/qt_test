#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkReply>
#include "populatenetworkdata.h"
#include "songitem.h"
#include "songitemmodel.h"
#include <QList>
#include <QSortFilterProxyModel>
#include <QMediaPlayer>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void playMusic(SongItem songItem);
    QMediaPlayer* player;
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QSortFilterProxyModel *songItemModelProxy;

private slots:
    void dataProgress(qint64 bytesReceived,qint64 bytesTotal);
    void on_lineEdit_textChanged(const QString &arg1);
    void on_listView_clicked(const QModelIndex &index);
    void slot_thumbFetched(QNetworkReply* reply);
    void slot_duration_changed(qint64 duration);
    void slot_volume_changed(qint64 position);
    void slot_position_changed(qint64 position);
    void on_playButton_clicked();
    void on_pauseButton_clicked();
    void slot_media_status_changed(QMediaPlayer::MediaStatus status);

    void on_seek_bar_sliderMoved(int position);
};

#endif // MAINWINDOW_H
