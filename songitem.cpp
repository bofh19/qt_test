#include "songitem.h"

SongItem::SongItem()
{

}

SongItem::SongItem(const SongItem &other)
{
    m_songDisplayName = other.getSongDisplayName();
    m_songUrl = other.getSongUrl();
    m_song_thumb_url = other.getSongThumbUrl();
    m_album = other.getAlbumName();
    m_artist = other.getArtist();
    m_name = other.getName();
}

SongItem::SongItem(SongItem* other)
{
    m_songDisplayName = other->getSongDisplayName();
    m_songUrl = other->getSongUrl();
    m_song_thumb_url = other->getSongThumbUrl();
    m_album = other->getAlbumName();
    m_artist = other->getArtist();
    m_name = other->getName();
}

QString SongItem::getSongDisplayName() const
{
    return m_songDisplayName;
}

void SongItem::setSongDisplayName(QString str)
{
    m_songDisplayName = str;
}

void SongItem::setSongUrl(QString url)
{
    m_songUrl = url;
}

QString SongItem::getSongUrl() const
{
    return m_songUrl;
}

QString SongItem::getSongThumbUrl() const
{
    return m_song_thumb_url;
}
void SongItem::setongThumbUrl(QString url)
{
    m_song_thumb_url = url;
}

QString SongItem::getAlbumName() const
{
    return m_album;
}
void SongItem::setAlbumName(QString album_name)
{
    album_name = m_album;
}

QString SongItem::getName() const
{
    return m_name;
}

void SongItem::setName(QString name)
{
    m_name = name;
}

QString SongItem::getArtist() const
{
    return m_artist;
}
void SongItem::setArtist(QString artist)
{
    m_artist = artist;
}

QString SongItem::getSongDisplayName2()
{
    return m_name + " - " + m_album + "("+m_artist+")";
}

QString SongItem::getServerSongThumbUrl()
{
    return "http://backpapermusic.w4rlock.in"+m_song_thumb_url;
}

QString SongItem::getServerSongUrl() const
{
    return "http://backpapermusic.w4rlock.in"+m_songUrl;
}

SongItem::~SongItem()
{

}

