#ifndef POPULATENETWORKDATA_H
#define POPULATENETWORKDATA_H
#include <QUrl>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QList>
#include "songitem.h"
class PopulateNetworkData :  public QObject
{
    Q_OBJECT
public:
    PopulateNetworkData();
    ~PopulateNetworkData();
    void getNetworkData(QUrl url);

private:
    QNetworkAccessManager *manager;

signals:
    void dataFetched(QList<SongItem*> songItems);
    void dataProgress(qint64 bytesReceived,qint64 bytesTotal);

public slots:
    void networkReplyFetched(QNetworkReply* reply);
    void testSlot(QString data);
};


#endif // POPULATENETWORKDATA_H
