#include "populatenetworkdata.h"
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonDocument>
#include "songitem.h"
#include <QList>
PopulateNetworkData::PopulateNetworkData()
{
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(networkReplyFetched(QNetworkReply*)));
}

PopulateNetworkData::~PopulateNetworkData()
{
    qDebug() << "destructor called";
}

void PopulateNetworkData::getNetworkData(QUrl url)
{
    qDebug() << "calling network request";
    QNetworkReply* reply = manager->get(QNetworkRequest(url));
    reply->setParent(this);
    connect(reply,SIGNAL(downloadProgress(qint64,qint64)),this,SIGNAL(dataProgress(qint64,qint64)));
}

//void PopulateNetworkData::downloadProgress(qint64 bytesReceived,qint64 bytesTotal)
//{
//    qDebug() << "bytesReceived " << bytesReceived << " bytesTotal "<<bytesTotal;
//}


void PopulateNetworkData::testSlot(QString data)
{
    qDebug() << "in test slot" << data;
}

void PopulateNetworkData::networkReplyFetched(QNetworkReply* reply)
{
    qDebug() << "in reply from populate network data class";

    QList<SongItem*> list;
    if (reply->error() == QNetworkReply::NoError)
    {
        qDebug() << "no errors recieved";
        QString data = (QString)reply->readAll();
        QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8());
        QJsonArray mainArray = doc.array();
        for(int i=0;i<mainArray.size();i++){
            QJsonArray arraytwo(mainArray.at(i).toArray());
            for(int j=0;j<arraytwo.size();j++){
                QJsonObject jsonObj(arraytwo.at(j).toObject());
                QJsonValue value1(jsonObj.value("name"));
                QJsonValue value2(jsonObj.value("track_url"));
                QJsonValue value3(jsonObj.value("album"));
                QJsonValue value4(jsonObj.value("thumb_url"));

                QJsonValue value5(jsonObj.value("album"));
                QJsonValue value6(jsonObj.value("name"));
                QJsonValue value7(jsonObj.value("artist"));

                QString name = value1.toString() +" / "+value2.toString();
                SongItem *song_item = new SongItem();
                song_item->setSongDisplayName(value3.toString()+" / "+value1.toString());
                song_item->setSongUrl(value2.toString());
                song_item->setongThumbUrl(value4.toString());
                song_item->setAlbumName(value5.toString());
                song_item->setName(value6.toString());
                song_item->setArtist(value7.toString());
                list.append(song_item);
                qDebug() << name << i << j << list.size();
            }
        }
        emit dataFetched(list);
        qDebug() << "model set";
    }
    else
    {
        qDebug() << "Errors recieved";
        qDebug() << reply->errorString();
        // handle errors here
    }
    delete reply;
}
