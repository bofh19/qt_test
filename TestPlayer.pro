#-------------------------------------------------
#
# Project created by QtCreator 2014-12-26T13:16:04
#
#-------------------------------------------------

QT       += core gui network multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TestPlayer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    songitem.cpp \
    songitemmodel.cpp \
    albumitem.cpp

HEADERS  += mainwindow.h \
    songitem.h \
    songitemmodel.h \
    albumitem.h

FORMS    += mainwindow.ui

ICON = myapp.ico
